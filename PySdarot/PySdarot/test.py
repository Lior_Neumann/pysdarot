from PySdarot import *

def main():
    sdarot = PySdarot()
    
    south = sdarot.s_list.south_park

    try:
        south.get_episode(5748, 2)
    except SeasonNotFound as ex:
        print "Error: %s" % ex.message
    
    playlist = PlayList()
    playlist.append(south.get_random_episode())
    playlist.append(south.get_random_episode())
    playlist.append(south.seasons.s_11.e_7)
    print south._img_url
    playlist.play()
    
    simp = sdarot.find_series("simp").the_simpsons
    try:
        simp.get_episode(21, 543)
    except EpisodeNotFound as ex:
        print "Error: %s" % ex.message


if __name__ == "__main__":
    main()