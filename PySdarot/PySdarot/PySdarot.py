import subprocess
import urllib
import requests
import os
import re
import random
import sys
from exceptions import Exception
import tempfile
from urllib3.util import connection

"""
This is an iPython interface for the well appreciated website http://www.sdarot.pm.
All the credit belongs to the website!
"""

BASE_URL = 'https://www.sdarot.tv'
AJAX_URL = BASE_URL + '/ajax'


_orig_create_connection = connection.create_connection


def patched_create_connection(address, *args, **kwargs):
    host, port = address

    if host.endswith("sdarot.tv"):
        host = "149.202.200.130" # Set Sdarot server IP statically
        
    return _orig_create_connection((host, port), *args, **kwargs)


connection.create_connection = patched_create_connection

"""
Exception class, representing episode not found error.
"""
class EpisodeNotFound(Exception):
    #Constants
    _EPISODE_NOT_AVIALABLE_FORMAT = 'Episode %s, Season %s is not avialable for "%s".'


    def __init__(self, episode, season, series_name):
        """
        Constructor

        episode: the episode number.
        season: the season number.
        series_name: the name of the series.
        """
        Exception.__init__(self, self._episode_not_avialable(episode, season, series_name))

    def _episode_not_avialable(self, episode, season, series_name):
        """
        Constructor

        episode: the episode number.
        season: the season number.
        series_name: the name of the series.
        """
        return self._EPISODE_NOT_AVIALABLE_FORMAT % (str(episode), str(season), series_name)


"""
Exception class, representing season not found error.
"""
class SeasonNotFound(Exception):
    #Constants
    _SEASON_NOT_AVIALABLE_FORMAT = 'Season %s is not avialable for "%s".'


    def __init__(self, season, series_name):
        """
        Constructor

        season: the season number.
        series_name: the name of the series.
        """
        Exception.__init__(self, self._season_not_avialable(season, series_name))

    def _season_not_avialable(self, season, series_name):
        """
        Creates exception string

        season: the season number.
        series_name: the name of the series.
        """
        return self._SEASON_NOT_AVIALABLE_FORMAT % (str(season), series_name)




"""
Base class, implmenting object that can be played.
"""
class Playable(object):
    
    @property
    def _path(self):
        raise NotImplementedError()

    def play(self):
        """
        Play the file.
        """
        if sys.platform == "win32":
            os.startfile(self._path)
        elif sys.platform == "darwin":
            subprocess.call(["open", self._path])
        else:
            subprocess.call(["xdg-open", self._path])


"""
Represent an epsiode after it was downloaded.
"""
class LocalEpisode(Playable):
    
    @property
    def _path(self):
        return self.__path

    def __init__(self, path):
        """
        Constructor

        path: the path of the episode on the fs.
        """
        self.__path = path


"""
Represent an Episode of some series, which can be played or downloaded.
"""
class Episode(Playable):
    #Constants
    _EPISODE_AJAX_URL = AJAX_URL + "/watch"
    _CHUNK_SIZE = 2048
    _PRINT_INTERVAL = 512
    _FILE_NAME_FORMAT = "%s/%s_%s_%s.mp4"
    _DOWNLOAD_DIR = '~/Downloads'
    _HD_VIDEO_FORMAT = 'http://%s/watch/episode/720/%s.mp4?token=%s&time=%d'
    _SD_VIDEO_FORMAT = 'http://%s/watch/episode/480/%s.mp4?token=%s&time=%d'
    
    @property
    def _path(self):
        self._load()
        return self._url
    
    def __eq__(self, other): return int(self._episode) == int(other._episode)
    def __ne__(self, other): return int(self._episode) != int(other._episode)
    def __lt__(self, other): return int(self._episode) < int(other._episode)
    def __le__(self, other): return int(self._episode) <= int(other._episode)
    def __gt__(self, other): return int(self._episode) > int(other._episode)
    def __ge__(self, other): return int(self._episode) >= int(other._episode)

    def __init__(self, series, season, episode):
        """
        Constructor

        e_url: the URL of the episode, which could be played.
        series: reference to the series which the episode belongs to.
        season: the season number of the episode.
        episode: the episode number.
        """
        Playable.__init__(self)
        self._loaded = False
        self._series = series
        self._season = season
        self._episode = episode
        self.download_dir = os.path.expanduser(self._DOWNLOAD_DIR)
        if not os.path.exists(self.download_dir):
            os.mkdir(self.download_dir)

    def _load(self):
        """
        Loads the episode using HTTP request, should be used only if absolutely necessary.
        """
        
        if not self._loaded:
            payload = {'watch' : 'true'}
            payload['serie'] = self._series._num
            payload['season'] = self._season
            payload['episode'] = self._episode
            payload['type'] = 'episode'
            payload['auth'] = 'undefined'
            payload['token'] = 'undefined'

            resp = requests.post(self._EPISODE_AJAX_URL, data = payload)
            resp_json = resp.json()

            if 'error' in resp_json:
                raise ValueError(resp_json['error'])

            if "720" in resp_json['watch']:
                self._url = self._HD_VIDEO_FORMAT % (resp_json['url'], resp_json['VID'], resp_json['watch']['720'], resp_json['time'])
            else:
                self._url = self._SD_VIDEO_FORMAT % (resp_json['url'], resp_json['VID'], resp_json['watch']['480'], resp_json['time'])
          
            self._loaded = True

    def download(self, local_filename = None):
        """
        Downloads the episode into local file.

        local_filename: the path of the file to be saved at.
        """
        
        if local_filename is None:
            local_filename = self._FILE_NAME_FORMAT % (self.download_dir, self._series._series_name, self._season, self._episode)

        r = requests.get(self._path, stream=True)
        file_size = int(r.headers['content-length'])
        i = 0
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=self._CHUNK_SIZE):
                if chunk:
                    f.write(chunk)
                    f.flush()
                    if (i % self._PRINT_INTERVAL) == 0:
                        downloaded = self._CHUNK_SIZE * i
                        print "\r%d%% [%d/%d]" % (int(float(downloaded) / file_size * 100), downloaded, file_size),
                        sys.stdout.flush()
                    i += 1
        print "\r100%% [%d/%d]" % (file_size, file_size)

        return LocalEpisode(local_filename)


"""
List of of playable objects, which can be played together.
"""
class PlayList(Playable):
    #Constants
    _PLAYLIST_EXTENSION = '.m3u'

    @property
    def _path(self):
        return self.__path

    def __init__(self, playlist = []):
        """
        Constructor

        playlist: initial list of playables.
        """
        self._playlist = playlist
        with tempfile.TemporaryFile(suffix = self._PLAYLIST_EXTENSION, delete = False) as file:
            self.__path = file.name
    
    def play(self):
        """
        Plays the playlist together.
        """
        self._refresh_file()
        Playable.play(self)

    def _refresh_file(self):
        """
        Rewrite the playlist file, accrosing to the object state.
        """
        with open(self._path, "w") as writer:
            writer.writelines(['%s\n' % p._path for p in self])

    def append(self, playable):
        """
        Appends item at the end of the playlist.

        playable: the item to append
        """
        self._playlist.append(playable)

    def insert(self, index, playable):
        """
        Insert item to the playlist at a given index.

        playable: the item to insert
        index: the index, which the item will be inserted in.
        """
        self._playlist.insert(index, playable)

    def pop(self, index=None):
        """
        Pops item at a given index.

        index: the index of the item to be poped.
        """
        return self._playlist.pop(index)

    def __getslice__(self, i, j):
        return PlayList(self._playlist[i:j])    

    def __iter__(self):
        return iter(self._playlist)

    def __getitem__(self, index):
        return self._playlist[index]




"""
Abstract class represent a collection of attributes, which can be used like a list.
"""
class AttrCollection(object):

    def __init__(self, attr_dict):
        """
        Constructor

        attr_dict: a dictionary of attributes to values.
        """
        self._values = []
        for k, v in attr_dict.items():
            setattr(self, k, v)
            self._values.append(v)
     
    def _attr_name(self, item):
        """
        Must implement function, which returns the name of the attribute, given a pseudo name

        item: the pseudo name.
        """
        raise NotImplementedError()

    def __getitem__(self, item):
        if isinstance(item, int):
            item = str(item)
        try:
            return getattr(self, self._attr_name(item))
        except AttributeError:
            return getattr(self, item)

    def __iter__(self):
        return iter(self._values)


"""
Represent a single season, which contains collection episodes.
"""
class Season(AttrCollection):
    #Constants
    _SEASON_AJAX_URL = AJAX_URL + "/watch"
    _EPISODE_PATTERN = 'data-episode="(\d*)"'
    _EPISODE_NAME_PATTERN = 'e_%s'

    def __init__(self, series, season_num):
        """
        Constructor

        series: the series which the season belongs to.
        season_num: the number of the season.
        """

        self._season_num = season_num
        self._series = series

        payload = (('episodeList', self._series._num), ('season', season_num))
        resp = requests.get(self._SEASON_AJAX_URL, params= payload)
        episodes = re.findall(self._EPISODE_PATTERN, resp.text)

        AttrCollection.__init__(self, {self._attr_name(e) : Episode(self._series, season_num, e) for e in episodes})

    def to_playlist(self):
        """
        Returns a playlist contains all of the episodes in the season.
        """
        return PlayList(sorted(self._values))
    
    def _attr_name(self, item):
        return self._EPISODE_NAME_PATTERN % item

    def __getitem__(self, item):
        try:
            return AttrCollection.__getitem__(self, item)
        except:
            raise EpisodeNotFound(item, self._season_num, self._series._series_name)


"""
Collection of seasons.
"""
class SeasonsList(AttrCollection):
    #Constants
    _SEASON_PATTERN = 'data-season="(\d*)"'
    _SEASON_NAME_PATTERN = 's_%s'

    def __init__(self, series):
        """
        Constructor

        series: the series which the seasons belong to.
        """
        
        self._series = series
        resp = requests.get(self._series._url)
        seasons = re.findall(self._SEASON_PATTERN, resp.text)
        AttrCollection.__init__(self, {self._attr_name(s) : Season(self._series, s) for s in seasons})        
  
    def _attr_name(self, item):
        return self._SEASON_NAME_PATTERN % item
    
    def __getitem__(self, item):
        try:
            return AttrCollection.__getitem__(self, item)
        except:
            raise SeasonNotFound(item, self._series._series_name)


"""
Collection of series.
"""
class SeriesList(AttrCollection):
    #Constants
    _ALL_SERIES_URL = AJAX_URL + '/index'
    _SERIES_NAME_ACCESS_REMOVE = "[%s]" % re.escape(r'+*.\'"()[]?!@#$~%^&<>/')

    def __init__(self, series_list=None):
        """
        Constructor:

        series_list: a list of series, which can be initialized, None for initialization using HTTP request.
        """

        series_dict = {}
        if series_list is None:
            payload = (('search', ' '),)
            resp = requests.get(self._ALL_SERIES_URL, params = payload)
            
            series_list = resp.json()
            for s in series_list:
                names = s['name'].split(' / ')
                heb, eng = names[0].replace(' ', '_'), names[-1].replace(' ', '_')
                series = Series(eng, heb, s['id'])
                
                series_dict[self._attr_name(series._series_name)] = series
        else:
            series_dict = {self._attr_name(series._series_name) : series for series in series_list}
        AttrCollection.__init__(self, series_dict)        

    def filter(self, substring):
        """
        Returns subset of the Series Collection, where the name contains a given substring.

        substring: substring to search.
        """
        return SeriesList([series for series in self if substring in series._series_name])

    def _attr_name(self, item):
        return re.sub(self._SERIES_NAME_ACCESS_REMOVE, '', item)




"""
Represents a series, which can be viewed
"""
class Series(object):
    #Constants
    _IMAGE_BASE_URL = "https://static.sdarot.tv/series/%s.jpg"
    _SERIES_BASE_URL = BASE_URL + "/watch/%s"

    @property
    def seasons(self):
        if self._seasons is None:
            self._update()
        return self._seasons

    @staticmethod
    def _is_ascii(s):
        return all(ord(c) < 128 for c in s)

    def __init__(self, eng, heb, num):
        """
        Constructor

        eng: The series name in English.
        heb: The series name in Hebrew.
        num: The number of the series.
        """
        self._url = self._SERIES_BASE_URL % num
        self._eng = eng
        self._heb = heb
        if Series._is_ascii(eng):
            self._series_name = eng.lower()
        else:
            self._series_name = num

        self._num = num
        self._img_url = self._IMAGE_BASE_URL % num
        self._seasons = None

    def get_episode(self, season, episode):
        """
        Get a certain episode of the series.

        season: the season number of the episode.
        episode: the episode number.
        """
        return self.seasons[season][episode]

    def get_season_playlist(self, season):
        """
        Get a playlist of all the episodes in a given season.
        """
        return self.seasons[season].to_playlist()

    def get_all_episodes(self):
        """
        Returns a list of the the preloaded episodes available for this series.
        """
        ret = []
        for s in self.seasons:
            ret.extend(sorted(s))
        return ret

    def get_random_episode(self):
        """
        Retruns a random episode in the series.
        """
        all_episodes = self.get_all_episodes()
        return all_episodes[random.randrange(len(all_episodes))]
    
    def _update(self):
        """
        Retrieves the episodes from the website, and update the seasons attribute accordingly.
        """
        self._seasons = SeasonsList(self)


"""
The main class, which can used to access all of the content
"""
class PySdarot(object):

    def __init__(self):
        """
        Constructor, also initializes the series cache, for further access.
        """        
        self.update_series_list()

    def update_series_list(self):
        """
        Updates the series cache.
        """
        self.s_list = SeriesList()

    def find_series(self, name):
        """
        Searches series in the database

        name: the name of the series (or part of it).
        """
        return self.s_list.filter(name)
